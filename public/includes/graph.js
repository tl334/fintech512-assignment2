var ready = () => {
    if (document.readyState != "loading") {
        listener();
    } else {
        document.addEventListener("DOMContentLoaded", listener);
    }
}

function listener() {
    document.getElementById("stock-select").addEventListener("change", startProcess);
    document.getElementById("submitBt").addEventListener("click", doubleConfirm);
}

function doubleConfirm() {
    bootbox.confirm({
        message: 'Do want to submit the page?',
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
    },
        callback: function (result) {
            if (result == false) {
                alert("Action Cancelled");
            } else {
                alert("Form Submitted");
            }
        }
    });
}

var stockNameG;

function startProcess() {
    var stock = document.getElementById("stock-select");
    var stockValue = stock.value;
    stockNameG = $( "#stock-select option:selected" ).text();
    makeplot(stockValue)
}

ready(() => {
  /* Do things after DOM has fully loaded */
});

function makeplot(stockValue) {
    console.log("makeplot: start")
    fetch("includes/data/" + stockValue +".csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")

};


function processData(data) {
  console.log("processData: start")
  let x = [], y = []

  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  }
  makePlotly( x, y );
  console.log("processData: end")
}

function makePlotly( x, y ){
  console.log("makePlotly: start")
  var traces = [{
        x: x,
        y: y
  }];
  var layout  = { title: stockNameG + " Stock Price History"}

  myDiv = document.getElementById('myDiv');
  Plotly.newPlot( myDiv, traces, layout );
  console.log("makePlotly: end")
};